<?php

interface StorageInterface
{
    public function getData($key);
    public function setData($key, $val);
}

interface DataAdapterInterface
{
    public function write($key, $val, $leafTime = 36000 );
    public function read($key);
}

class MemcacheAdapter implements DataAdapterInterface
{
    /** @var Memcached  */
    private $adapter;

    public function __construct($host = '127.0.0.1', $port = 11211)
    {
        $this->adapter = new Memcached('my_server');
        $this->adapter->addServer($host, $port);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function read($key)
    {
        return $this->adapter->get($key);
    }

    /**
     * @param string $key Cache key
     * @param mixed $val
     * @param int $leafTime Variable leaf time in second
     */
    public function write($key, $val, $leafTime = 36000)
    {
        $this->adapter->set($key, $val, $leafTime);
    }
}

class MemoryStorage implements StorageInterface
{
    /**
     * @var
     */
    private $dataAdapter;

    public function __construct(DataAdapterInterface $dataAdapter)
    {
        $this->dataAdapter = $dataAdapter;
    }

    public function setData($key, $val)
    {
        $this->dataAdapter->write($key, $val);
    }

    public function getData($key)
    {
        return $this->dataAdapter->read($key);
    }

}